/**
 * Represents a bird in the bird watcher application.
 * Each bird has an ID, type, position, speed, angle, and an image.
 */
export class Bird {
  id: number;  // Unique identifier for the bird
  type: number;  // Type of the bird (randomly assigned between 1 and 5)
  x: number;  // X-coordinate of the bird's position
  y: number;  // Y-coordinate of the bird's position
  speedX: number;  // Horizontal speed of the bird
  speedY: number;  // Vertical speed of the bird
  angle: number;  // Angle of the bird's movement (calculated from speed)
  image: HTMLImageElement;  // Image element representing the bird

  /**
   * Constructs a new Bird instance.
   * @param id - Unique identifier for the bird.
   * @param canvasWidth - Width of the canvas (used for initial position).
   * @param canvasHeight - Height of the canvas (used for initial position).
   */
  constructor(id: number, canvasWidth: number, canvasHeight: number) {
    this.id = id;
    this.type = Math.floor(Math.random() * 5) + 1;  // Randomly assign bird type (1 to 5)

    // Randomly assign initial position and speed based on the side the bird starts from
    const side = Math.floor(Math.random() * 4);
    switch (side) {
      case 0: // Left side of the canvas
        this.x = 0;
        this.y = Math.random() * canvasHeight;
        this.speedX = Math.random() * 2 + 1;
        this.speedY = (Math.random() - 0.5) * 2;
        break;
      case 1: // Top side of the canvas
        this.x = Math.random() * canvasWidth;
        this.y = 0;
        this.speedX = (Math.random() - 0.5) * 2;
        this.speedY = Math.random() * 2 + 1;
        break;
      case 2: // Right side of the canvas
        this.x = canvasWidth;
        this.y = Math.random() * canvasHeight;
        this.speedX = -(Math.random() * 2 + 1);
        this.speedY = (Math.random() - 0.5) * 2;
        break;
      case 3: // Bottom side of the canvas
        this.x = Math.random() * canvasWidth;
        this.y = canvasHeight;
        this.speedX = (Math.random() - 0.5) * 2;
        this.speedY = -(Math.random() * 2 + 1);
        break;
      default:
        throw new Error('Invalid side value');
    }

    // Calculate the angle of movement based on speed
    this.angle = Math.atan2(this.speedY, this.speedX);
    
    // Load the image for the bird
    this.image = new Image();
    this.image.src = `bird${this.type}.gif`;
  }

  /**
   * Updates the bird's position based on its speed.
   * @returns A boolean indicating whether the bird is still within the canvas bounds.
   */
  updatePosition(): boolean {
    // Update the position
    this.x += this.speedX;
    this.y += this.speedY;

    // Check if the bird is still within the canvas bounds
    return !(this.x < 0 || this.x > window.innerWidth || this.y < 0 || this.y > window.innerHeight);
  }
}
