import { TestBed } from '@angular/core/testing';
import { BirdWatcherService } from './bird-watcher.service';

describe('BirdWatcherService', () => {
  let service: BirdWatcherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BirdWatcherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getMostFrequentSighting', () => {
    it('should return -1 for an empty array', () => {
      expect(service.getMostFrequentSighting([])).toBe(-1);
    });

    it('should return the only element for an array with one element', () => {
      expect(service.getMostFrequentSighting([1])).toBe(1);
    });

    it('should return the most frequent element', () => {
      expect(service.getMostFrequentSighting([1, 2, 2, 3, 3, 3])).toBe(3);
    });

    it('should return the smallest element if there is a tie in frequency', () => {
      expect(service.getMostFrequentSighting([1, 2, 2, 3, 3])).toBe(2);
    });

    it('should handle large datasets efficiently', () => {
      const largeArray = Array.from({ length: 1_000_000 }, (_, i) => i % 10);
      const start = performance.now();
      const result = service.getMostFrequentSighting(largeArray);
      const end = performance.now();
      console.log(`Performance test took ${end - start} milliseconds`);
      expect(result).toBe(0);
    });
  });

  describe('getLatestFirstSighting', () => {
    it('should return -1 for an empty array', () => {
      expect(service.getLatestFirstSighting([])).toBe(-1);
    });

    it('should return the only element for an array with one element', () => {
      expect(service.getLatestFirstSighting([1])).toBe(1);
    });

    it('should return the latest first sighting element', () => {
      expect(service.getLatestFirstSighting([1, 2, 3, 4, 5, 2, 3, 4, 1])).toBe(5);
    });

    it('should handle arrays with all identical elements', () => {
      expect(service.getLatestFirstSighting([1, 1, 1, 1])).toBe(1);
    });

    it('should handle large datasets efficiently', () => {
      const largeArray = Array.from({ length: 1_000_000 }, (_, i) => i % 10);
      const start = performance.now();
      const result = service.getLatestFirstSighting(largeArray);
      const end = performance.now();
      console.log(`Performance test took ${end - start} milliseconds`);
      expect(result).toBe(9);
    });
  });
});
