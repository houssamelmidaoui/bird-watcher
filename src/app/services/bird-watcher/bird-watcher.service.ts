import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BirdWatcherService {

  /**
   * Determines the most frequently sighted bird in the given array.
   * If there is a tie in frequency, it returns the bird with the smallest ID.
   * 
   * @param birds Array of bird type IDs.
   * @returns The ID of the most frequently sighted bird, or -1 if the array is empty.
   */
  getMostFrequentSighting(birds: number[]): number {
    // Return -1 if the input array is empty
    if (birds.length === 0) return -1;

    // Create a Map to store the frequency of each bird type ID
    const frequency = new Map<number, number>();
    // Initialize the most frequent bird ID and its frequency
    let mostFrequentBird = birds[0];
    let maxFrequency = 1;

    // Iterate over each bird type ID in the array
    for (const bird of birds) {
      // Update the frequency count for the current bird ID
      const count = (frequency.get(bird) || 0) + 1;
      frequency.set(bird, count);

      // Update the most frequent bird ID if necessary
      if (count > maxFrequency || (count === maxFrequency && bird < mostFrequentBird)) {
        maxFrequency = count;
        mostFrequentBird = bird;
      }
    }

    // Return the ID of the most frequently sighted bird
    return mostFrequentBird;
  }

  /**
   * Determines the bird that was first sighted most recently.
   * 
   * @param birds Array of bird type IDs.
   * @returns The ID of the bird that was first sighted last, or -1 if the array is empty.
   */
  getLatestFirstSighting(birds: number[]): number {
    // Return -1 if the input array is empty
    if (birds.length === 0) return -1;

    // Create a Map to store the first sighting index of each bird type ID
    const firstSightingIndex = new Map<number, number>();
    // Initialize the latest first sighting bird ID and its index
    let latestFirstSightingBird = birds[0];
    let latestFirstSightingIndex = 0;

    // Iterate over each bird type ID in the array
    for (let i = 0; i < birds.length; i++) {
      // If the current bird ID is seen for the first time, store its index
      if (!firstSightingIndex.has(birds[i])) {
        firstSightingIndex.set(birds[i], i);

        // Update the latest first sighting bird ID if necessary
        if (i > latestFirstSightingIndex) {
          latestFirstSightingIndex = i;
          latestFirstSightingBird = birds[i];
        }
      }
    }

    // Return the ID of the bird that was first sighted most recently
    return latestFirstSightingBird;
  }
}
