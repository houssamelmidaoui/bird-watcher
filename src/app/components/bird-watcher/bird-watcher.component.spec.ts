import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BirdWatcherComponent } from './bird-watcher.component';
import { BirdWatcherService } from '../../services/bird-watcher/bird-watcher.service';
import { By } from '@angular/platform-browser';

describe('BirdWatcherComponent', () => {
  let component: BirdWatcherComponent;
  let fixture: ComponentFixture<BirdWatcherComponent>;
  let birdWatcherService: BirdWatcherService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BirdWatcherComponent], // Import the standalone component
      providers: [BirdWatcherService]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BirdWatcherComponent);
    component = fixture.componentInstance;
    birdWatcherService = TestBed.inject(BirdWatcherService);
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should start and stop recording', () => {
    spyOn(birdWatcherService, 'getMostFrequentSighting').and.returnValue(1);
    spyOn(birdWatcherService, 'getLatestFirstSighting').and.returnValue(1);

    const button = fixture.debugElement.query(By.css('button')).nativeElement;
    
    button.click();  // Start recording
    expect(component.recording).toBeTrue();

    component.birdsTypes.push(1); // Simulate recording a bird type

    button.click();  // Stop recording
    expect(component.recording).toBeFalse();
    expect(component.mostFrequentBird).toBe(1);
    expect(component.latestFirstSightingBird).toBe(1);
  });

  it('should display statistics correctly', () => {
    component.mostFrequentBird = 2;
    component.latestFirstSightingBird = 3;
    fixture.detectChanges();

    const stats = fixture.debugElement.query(By.css('.stats-panel mat-card-content')).nativeElement;
    expect(stats.textContent).toContain('Most Frequent Bird: 2');
    expect(stats.textContent).toContain('Latest First Sighting Bird: 3');
  });
});
