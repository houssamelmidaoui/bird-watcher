import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BirdWatcherService } from '../../services/bird-watcher/bird-watcher.service';
import { Bird } from '../../models/bird/bird.model';
import { MaterialModule } from '../../material.module';

/**
 * BirdWatcherComponent is responsible for managing the bird watching functionality.
 * It handles bird generation, animation, recording sightings, and displaying statistics.
 */
@Component({
  selector: 'app-bird-watcher',
  standalone: true,
  imports: [CommonModule, MaterialModule],
  templateUrl: './bird-watcher.component.html',
  styleUrls: ['./bird-watcher.component.css'],
  providers: [BirdWatcherService]
})
export class BirdWatcherComponent implements OnInit, OnDestroy {
  private birds: Bird[] = [];  // Array to store the generated birds
  public birdsTypes: number[] = [];  // Array to store the types of recorded birds
  public recording: boolean = false;  // Flag to indicate if recording is active
  private birdGenerationInterval: any;  // Interval ID for bird generation
  public mostFrequentBird: number | null = null;  // Most frequently sighted bird
  public latestFirstSightingBird: number | null = null;  // Latest first sighting bird
  private birdIdCounter = 0;  // Counter to assign unique IDs to birds
  private canvas!: HTMLCanvasElement;  // Reference to the canvas element
  private ctx!: CanvasRenderingContext2D;  // Canvas rendering context

  constructor(private birdWatcherService: BirdWatcherService) {}

  /**
   * Initializes the component by setting up the canvas.
   */
  ngOnInit(): void {
    this.initCanvas();
  }

  /**
   * Cleans up resources when the component is destroyed.
   */
  ngOnDestroy(): void {
    this.cleanup();
  }

  /**
   * Initializes the canvas and starts the bird animation.
   */
  private initCanvas(): void {
    this.canvas = document.getElementById('birdCanvas') as HTMLCanvasElement;
    this.ctx = this.canvas.getContext('2d')!;
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.animateBirds();
  }

  /**
   * Animates the birds by updating their positions and redrawing them.
   */
  private animateBirds(): void {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.birds = this.birds.filter(bird => {
      const isInBounds = bird.updatePosition();
      if (isInBounds) {
        this.drawBird(bird);
      }
      return isInBounds;
    });

    requestAnimationFrame(() => this.animateBirds());
  }

  /**
   * Draws a bird on the canvas.
   * @param bird The bird to be drawn.
   */
  private drawBird(bird: Bird): void {
    const birdWidth = 50;
    const birdHeight = 50;
    this.ctx.save();
    this.ctx.translate(bird.x, bird.y);
    this.ctx.rotate(bird.angle);
    this.ctx.drawImage(bird.image, -birdWidth / 2, -birdHeight / 2, birdWidth, birdHeight);
    this.ctx.restore();
  }

  /**
   * Generates a new bird and adds it to the list if recording is active.
   */
  private generateBird(): void {
    try {
      const bird = new Bird(this.birdIdCounter++, window.innerWidth, window.innerHeight);
      if (this.recording) {
        this.birds.push(bird);
        this.birdsTypes.push(bird.type);
      }
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Toggles the recording state and starts or stops recording accordingly.
   */
  public toggleRecording(): void {
    this.recording = !this.recording;
    this.recording ? this.startRecording() : this.stopRecording();
  }

  /**
   * Starts recording bird sightings by generating birds at intervals.
   */
  private startRecording(): void {
    this.mostFrequentBird = null;
    this.latestFirstSightingBird = null;
    this.birdGenerationInterval = setInterval(() => this.generateBird(), 250);
  }

  /**
   * Stops recording bird sightings and calculates statistics.
   */
  private stopRecording(): void {
    if (this.birdGenerationInterval) {
      clearInterval(this.birdGenerationInterval);
    }
    this.calculateStatistics();
    this.clearBirds();
  }

  /**
   * Calculates and updates the most frequently sighted bird and the latest first sighting bird.
   */
  private calculateStatistics(): void {
    this.mostFrequentBird = this.birdWatcherService.getMostFrequentSighting(this.birdsTypes);
    this.latestFirstSightingBird = this.birdWatcherService.getLatestFirstSighting(this.birdsTypes);
  }

  /**
   * Clears the list of birds and bird types.
   */
  private clearBirds(): void {
    this.birds = [];
    this.birdsTypes = [];
  }

  /**
   * Cleans up resources by stopping recording and clearing the interval.
   */
  private cleanup(): void {
    this.stopRecording();
    if (this.birdGenerationInterval) {
      clearInterval(this.birdGenerationInterval);
    }
  }
}
