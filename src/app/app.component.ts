import { Component } from '@angular/core';
import { BirdWatcherComponent } from './components/bird-watcher/bird-watcher.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [BirdWatcherComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = "bird-watcher-app";
}
