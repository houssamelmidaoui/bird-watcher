# Bird Watcher Application

## Overview

The Bird Watcher Application is an interactive web application designed to simulate bird watching activities. Users can start and stop recording bird sightings as various birds fly across the screen. The application keeps track of the types of birds seen and provides statistics such as the most frequently sighted bird and the bird that was spotted for the first time at the latest moment.

## Features

- **Most Frequent Sighting**: Determines the ID of the most frequently sighted bird type.
- **Latest First Sighting**: Identifies the bird type that was spotted for the first time at the latest moment.
- **Bird Animation**: Birds of different types randomly appear on the screen and move in various directions.
- **Recording Functionality**: Users can start and stop recording bird sightings. The types of birds seen are stored and analyzed.
- **Responsive Design**: Ensures the application is accessible and functional on various devices.

## Technology Stack

- **Frontend**:
  - Angular: TypeScript-based frontend framework.
  - Angular Material: UI components for responsive design.

## Getting Started

Follow these instructions to set up the project locally.

### Prerequisites

Ensure you have the following installed on your local machine:

- Node.js (v20.13.1)
- npm (v10.5.2)
- Angular CLI (v18.0.1)

### Installation

1. **Clone the repository**:
   ```bash
   git clone https://gitlab.com/houssamelmidaoui/bird-watcher.git

2. **Navigate to the project directory**:
   ```bash
   cd bird-watcher-app

3. **Install dependecies**:
   ```bash
   npm install

### Running the application

- **Start the development server:**:
  ```bash
   ng serve

This will compile the application and start a local development server. By default, it will be accessible at http://localhost:4200/.

**Open your browser and navigate to http://localhost:4200/ to view the application.**

### Running Tests

- **Unit and Integration Tests:**
  ```bash
   ng test

This will run the unit and integration tests using Karma and Jasmine. A browser window will open displaying the test results.

### Project structure

- **src/app**
  - **components**: Contains the main BirdWatcherComponent.
  - **models**: Contains the Bird model.
  - **services**: Contains the BirdWatcherService.
- **public**: Contains static assets such as bird images.

### Code documentation

The project includes comprehensive comments within the codebase to ensure maintainability and clarity.





